/*
 * Ahoy.c
 *
 * Created: 14/09/2020 14:32:04 

 * TilerCharge
 *
 * 
 * Author : Grad Anamaria
 *
 *Auto-restart function, overcurrent protection,
 *
 * power good timer for starting!!
 */ 

//PA0 UPDI 
//PA1 output status for platform leds
//PA2 staus bluetooth (input)
//PA3 
//PA4 
//PA5 
//PA6 OCD threshold  
//PA7 read NTC (input)
//PB0 
//PB1 
//PB2 TX
//PB3 RX
//PB4
//PB5
//PB6 SW (button)
//PB7 LED_G
//PC0 P1 start
//PC1 P2 start
//PC2 EN IR2110
//PC3 OCD input
//PC4 LED_R
//PC5 LED_Y
//VDD Power Supply
//GND GND



#define F_CPU 3333333
#define USART0_BAUD_RATE(BAUD_RATE) ((float)(F_CPU * 64 / (16 * (float)BAUD_RATE)) + 0.5)
#define MAX_COMMAND_LEN 10

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>

uint16_t current_tick=0,pwrgood_time=0;
uint8_t blanking_time=0, start=0, status_battery=0, ocd=0, flag=0, index=0, a=25; //a=24-> f=131k ; a= 26 135k, a=20= 125k, a=28 -> 139k, a=22-> f 128k
char command[MAX_COMMAND_LEN];
char c;


#define LED_RED_ON  PORTC.OUT &= ~(PIN4_bm)
#define LED_RED_OFF  PORTC.OUT |= (PIN4_bm)

#define LED_YELLOW_ON  PORTC.OUT &= ~(PIN5_bm)
#define LED_YELLOW_OFF  PORTC.OUT |= (PIN5_bm)

#define LED_GREEN_ON  PORTB.OUT &= ~(PIN7_bm)
#define LED_GREEN_OFF  PORTB.OUT |= (PIN7_bm)

#define DISABLE_DRIVER PORTC.OUT &= ~(PIN2_bm)
#define ENABLE_DRIVER PORTC.OUT |= (PIN2_bm)

#define SIGNAL_PERIOD_VALUE            (0x64-a)
#define SIGNAL_DUTY_CYCLE_VALUE        (0x64-a)/2

void USART0_init(void);
void USART0_sendChar(char c);
void USART0_sendString(char *str);
char USART0_readChar(void);
void executeCommand(char *command);


//overcurrent situation interrupt
ISR(PORTC_PORT_vect)
{
	cli();//disable interrupts
	PORTC.INTFLAGS |= PIN3_bm;	//clear flag PC3
	
	if(blanking_time==0)	//if blanking_time time is over
	{
		DISABLE_DRIVER;	//disable irs2110 driver;
		ocd=1;
		LED_RED_ON;	//turn on led red
	    PORTA.OUT&= ~ PIN1_bm;  //PA1=0 no charging
		
	}
	else
	{
		ocd=0;
	}
	sei();//enable interrupts
}



//clkticks isr
ISR (TCA0_OVF_vect)
{
	TCA0.SINGLE.INTFLAGS |= TCA_SINGLE_OVF_bm;	//clear flag
	current_tick++;//free running counter
	if(blanking_time)	//as long as blanking_time is not 0;
	{
		blanking_time--;	//countdown blanking_time
	}
	if(current_tick==10000) //add 5000 ticks to blink at 1sec intervals (2sec intervals 10000 ticks)
	{
		current_tick=0;
		flag=1;
	}
}

ISR (USART0_RXC_vect)
{ 
	cli();
	 char c = USART0_readChar();
	 if(c != '\n' && c != '\r')
	 {
		 command[index++] = c;
		 if(index > MAX_COMMAND_LEN)
		 {
			 index = 0;
		 }
	 }
	 
	 if(c == '\n')
	 {
		 command[index] = '\0';
		 index = 0;
		 executeCommand(command);
	 }
	 sei();
}



void USART0_init(void)
{
	PORTB.DIR &= ~PIN3_bm; //RX pin configure as input
	PORTB.DIR |= PIN2_bm; //TX pin configure as output
	
	USART0.BAUD = (uint16_t)USART0_BAUD_RATE(115200); //set the baud rate

	USART0.CTRLB |= USART_RXEN_bm | USART_TXEN_bm;
	USART0.CTRLA |=USART_RXCIE_bm; //enable the transmitter and receiver, enable the receiver interrupt
	
}

void USART0_sendChar(char c) //check if the previous transmission is complete by checking the STATUS register.
//waits until the transmit DATA register is empty and then writes a character to TXDATA register
{
	while (!(USART0.STATUS & USART_DREIF_bm))
	{
		;
	}
	USART0.TXDATAL = c;
}

void USART0_sendString(char *str) //this function calls the USART0_sendCharacter function for each character
{
	for(size_t i = 0; i < strlen(str); i++)
	{
		USART0_sendChar(str[i]);
	}
}

char USART0_readChar(void) //wait for the data to be available by polling the receiver complete flag
{
	while (!(USART0.STATUS & USART_RXCIF_bm))
	{
		;
	}
	return USART0.RXDATAL;
}

void executeCommand(char *command)
{  
	if(strcmp(command,"NoCharge") == 0)
	{
		status_battery++; //try 5 times

	}
    if (strcmp(command,"BattFault") == 0)
	{   
		DISABLE_DRIVER;
		PORTA.OUT&= ~ PIN1_bm;  //PA1=0 no charging
		_delay_ms(100000); //1 minut tested
		start=1;
		
	}
	if(strcmp(command,"NTCFault") == 0)
	{    
			DISABLE_DRIVER;
			PORTA.OUT&= ~ PIN1_bm;  //PA1=0 no charging	
			_delay_ms(100000); //1 minut tested
			start=1;
		
	}
}

	void Init_pins (void)
	{
		//OCD threshold- output
		PORTA.DIR |= (PIN6_bm);   //set PA6 as output
		//Switch input
		PORTB.DIR &= ~(PIN6_bm);   //set PB6 as input
		//Connection bluetooth status
		PORTA.DIR &= ~(PIN2_bm);   //PA2 set as input
		//-platform LED
		PORTA.DIR |= PIN1_bm; //PA1 output
		PORTA.OUT |= PIN1_bm;;  //PA1=1 initialization
		//P1 start- output
		PORTC.DIR |= PIN0_bm; //PC0 output 
		PORTC.OUT |= PIN0_bm;  //PC0=1
		//P2 start- output
		PORTC.DIR |= PIN1_bm;  //PC1 output
		PORTC.OUT &= ~(PIN1_bm);  //PC1=0
		//IRS2110 driver enable output: LOW is OFF, High is ENABLE;
		PORTC.DIR |= PIN2_bm; //set PC2 as output
	   //OCD input
	    PORTC.DIR &= ~(PIN3_bm);   //set PC3 as input	
		//LED_R output;
		PORTC.DIR |= PIN4_bm; //set PC4 as output
		LED_RED_OFF;	
		//LED_Y output;
		PORTC.DIR |= PIN5_bm; //set PC5 as output
		LED_YELLOW_OFF;	
		//LED_G output;
		PORTB.DIR |= PIN7_bm; //set PB7 as output
		LED_GREEN_OFF;
	}


		
	void P1_P2_Pulses_init (void)
		{
			/* set the waveform mode */
    TCD0.CTRLB = TCD_WGMODE_DS_gc;
    
    /* set the signal period */
    TCD0.CMPBCLR = SIGNAL_PERIOD_VALUE;    
    
    /* the signals are alternatively active and a small
       symmetric dead time is needed */
    TCD0.CMPBSET = SIGNAL_DUTY_CYCLE_VALUE;
    TCD0.CMPASET = SIGNAL_DUTY_CYCLE_VALUE;
	TCD0.CTRLC |= TCD_CMPDSEL_bm; // connect WOD to WOB ( WOC connected to WOA by default)
	CPU_CCP |= CCP_IOREG_gc;	//enable possibility to connect outputs to pins
	TCD0.FAULTCTRL |= TCD_CMPCEN_bm | TCD_CMPDEN_bm;	//connect output to PC0 and PC1
      /* ensure ENRDY bit is set */
      while(!(TCD0.STATUS & TCD_ENRDY_bm))
      {
	      ;
      }
      
      TCD0.CTRLA = TCD_CLKSEL_20MHZ_gc        /* choose the timer's clock */
      | TCD_CNTPRES_DIV1_gc;        /* choose the prescaler */
                		
	}
	
	void OCD_interrupt_enable (void)
	{
		//init pin change interrupt on PC3
		PORTC.INTFLAGS |= PIN3_bm;	//PCINT on PC3
		PORTC.PIN3CTRL |= PORT_ISC_RISING_gc;	//interrupts on rising edge only	
	}
	
	
	//init TCA0 (used for generating clk ticks)
	void Generate_clk_ticks (void)
	{	
		 //set the waveform mode to single slope and enable the compare register 0 and 1
		TCA0.SINGLE.CTRLA |= (TCA_SINGLE_CLKSEL_DIV8_gc)|(TCA_SINGLE_ENABLE_bm);	//prescale 8  enable tca
		TCA0.SINGLE.PER	= 40;	//~0.1ms
		TCA0.SINGLE.INTCTRL |= (TCA_SINGLE_OVF_bm);	//enable overflow interrupt
}

	
void OCD_threshold(void)
	{
	//init DAC and internally enable output to PA6
	VREF.CTRLA |= VREF_DAC0REFSEL_1V1_gc;//1v1Vref for dac
	DAC0.CTRLA|= (DAC_ENABLE_bm) | (DAC_OUTEN_bm); // DAC enable and output enable to PA6
	DAC0.DATA =220 ;   // ~3.5A current limit
	}

void Powergood ()

	{   
		_delay_ms(1000); // delay 1s 
		
		start=1; //automatically start process.
		
		
	}
	
	void Start_pulses( uint16_t blank_OCD, int8_t nr_of_pulses)
	{
		if(start) //produce a single train of start pulses per button press
		{
			blanking_time = blank_OCD; //blanking_time ocd at startup for 4ms
			ENABLE_DRIVER;	//only enable driver at first press so shutdown can occur via isr
			TCD0.CTRLA |= TCD_ENABLE_bm; //enable tcd0 timer
			for (int8_t i=0;i<nr_of_pulses;i++)	//for 5 tcd0 periods
			{
				while (!(TCD0.INTFLAGS & TCD_OVF_bm))//wait for ovf flag
				{
					;
				}
				TCD0.INTFLAGS = TCD_OVF_bm;//clear flag
			}
			start=0;//train left the station, next train on next button press or start automatically if a ocd is occured
			TCD0.CTRLA &= ~(TCD_ENABLE_bm); 	//disable tcd0 timer		
			
	}
	}
		void P1_P2_Pulses (void)
		{
			/* ensure ENRDY bit is set */
			while(!(TCD0.STATUS & TCD_ENRDY_bm))
			{
				;
			}
			
			TCD0.CTRLA = TCD_CLKSEL_20MHZ_gc        /* choose the timer's clock */
			| TCD_CNTPRES_DIV1_gc        /* choose the prescaler */
			| TCD_ENABLE_bm;             /* enable the timer */
		}
int main (void)

{   
	int8_t toggle=0;

    USART0_init();
	Init_pins(); //set pins as input and/or output
	P1_P2_Pulses_init(); //init timer TCD
	
	OCD_threshold(); // set the threshold for the OCD
	Generate_clk_ticks(); // timer TCA generate clk 10kHz/ 0.1ms
	
	sei();	//enable global interrupts
	Powergood (); 
    OCD_interrupt_enable(); //enable interrupt
	
	while (1)
	{	
		
		Start_pulses(60,5); //40*0.1ms blanking time, 5 -nr of pulses
	    P1_P2_Pulses ();
		 if(flag == 1)
		 {   
				   if(PORTA.IN & PIN2_bm)//PA2=1, bluetooth connected
				     {   	 
						 LED_YELLOW_OFF;
						 ENABLE_DRIVER;
						 PORTA.OUT |= PIN1_bm;  //PA1=1   charging	
					 }
					 if((PORTA.IN & PIN2_bm)&&(status_battery==5))//PA2=1, bluetooth connected
					 {
						status_battery=0;
						DISABLE_DRIVER;
						PORTA.OUT&= ~ PIN1_bm;  //PA1=0 no charging		
						_delay_ms(100000); //1 minut tested
						start=1;
					 }
					 
					if (!(PORTA.IN & PIN2_bm))	
					 {
						 LED_YELLOW_ON;
						 DISABLE_DRIVER;
						 PORTA.OUT&= ~ PIN1_bm;  //PA1=0 no charging				 
					 }
			   
			 flag=0;
			 
			 if(toggle)
			 {   
				 LED_GREEN_ON;	
				 if((ocd)&&(PORTA.IN & PIN2_bm)) //reset start if ocd occurred
				 {
					 status_battery=0;
					 start=1;
					 LED_RED_OFF;
					
				 }
				 else
				 {
					 start=0;
				
				 }
				 toggle=0;
			  }
			 else
			  {
				 LED_GREEN_OFF;	
				 toggle=1;
				 if (!(PORTB.IN & PIN6_bm)) //if switch is pressed//if switch is pressed
				 
				 {
				     start=1;	//manual way to start without ocd.
					
				 }
			  }
	      }
	  }
  }

	







